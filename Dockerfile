# syntax = docker/dockerfile:1.2
FROM zricethezav/gitleaks:v8.8.12
LABEL maintener="cpontvieux@systra.com"
ENV LANG=C.UTF8
USER root
RUN apk update \
 && apk upgrade \
 && apk add py3-pip \
 && pip install pipenv
COPY ./entrypoint /usr/local/bin/entrypoint
ENTRYPOINT ["/usr/local/bin/entrypoint"]
RUN mkdir /opt/gitleaks && chown gitleaks: /opt/gitleaks
USER gitleaks
WORKDIR /opt/gitleaks
COPY ./Pipfile ./Pipfile.lock ./mergetoml ./systraleaks.toml ./
RUN pipenv sync \
 && wget https://github.com/zricethezav/gitleaks/raw/master/config/gitleaks.toml
ENV APP_DIR=/app LOG_OPTS="--branches --tags"
VOLUME /app
