#!/bin/sh
set -e
cd "$(dirname "$0")"
current_ver=$(cat version)
major=$(echo "$current_ver"|cut -d. -f1)
minor=$(echo "$current_ver"|cut -d. -f2)
patch=$(echo "$current_ver"|cut -d. -f3)
if [ -n "$1" ]; then
  update="$1"
else
  changes=$(sed -n '/^## Next/,/^## /{/^###/s/### //p}' CHANGELOG.md)
  if echo "$changes"|grep -q 'Changed'; then
    update_letter=''
    while [ "$update_letter" != "M" ] && [ "$update_letter" != "m" ]; do
      sed -n '/^## Next/,/^## /p' CHANGELOG.md|head -n-1
      printf "Is this a [M]ajor or [m]inor release? "
      read -r update_letter
    done
    [ "$update_letter" = "M" ] && update='major' || update='minor'
  elif [ "$changes" = "Fixed" ]; then
    update='patch'
  else
    update='minor'
  fi
fi
if [ "$update" = "major" ]; then
  major=$((major + 1))
  minor=0
  patch=0
elif [ "$update" = "minor" ]; then
  minor=$((minor + 1))
  patch=0
else
  patch=$((patch + 1))
fi
next_ver=$major.$minor.$patch
echo " -> $next_ver"
echo "$next_ver" > version
sed -ri 's/^## Next$/## '$next_ver'/' CHANGELOG.md
sed -ri "s/:$current_ver/:$next_ver/" README.md
git add version CHANGELOG.md README.md
git commit -m "version $next_ver"
git tag -a -f -m "version $next_ver" v$next_ver
